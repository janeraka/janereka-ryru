export default {
  about: {
    pageTextEthnicity: 'A etnia', // Pending translation
    pageTextAboutAwaeteCitationA: 'yuwy we araka kumeti u\'u tipe gy ure renuemi uékujawi tipe ure\nGyawi assirini i\'i urewe awaete ramu ure mywe jepe\nAraka mywe jepe awa jay\'yma já tapy\'yia ei ure mumawa mywe jepe\nMarytyka ujeup tawyipe mywe jepe\nAeramu má mawa umama e retxaukwata ra\nTapy\'yia juaka awe uta gy pyri mywe marytykaramera',
    pageTextAboutAwaeteCitationB: 'Gyawi uterenui asoreni jawu\nAwa amute awi gy uetcak pajau',
    pageTextAboutAwaeteCitationBibliography: 'NIMUENDAJÚ, 1948, p. 255; RIBEIRO, 2009, p. 61',
    pageTitleAboutDenomination: 'AUTO DENOMINAÇÃO',
    pageTitleAboutLanguage: 'LÍNGUA',
    pageTitleAboutIndigenousLand: 'TERRA INDÍGENA',
    pageTitleAboutArea: 'ÁREA OFICIAL',
    pageTitleAboutStatus: 'STATUS TERRITORIAL',
    pageTitleAboutUnit: 'UNIDADE ADM. FUNAI',
    pageTitleAboutBiome: 'BIOMA',
    pageTitleAboutBasin: 'BACIA HIDROGRÁFICA',
    pageTitleAboutVegetal: 'COBERTURA VEGETAL',
    pageTitleAboutIndiginousGroup: 'ALDEIAS',
    pageTitleAboutPopulation: 'POPULAÇÃO',
    pageTitleAboutKnowledge: 'PRINCIPAIS SABERES TRADICIONAIS',
    pageTitleAboutThePlatform: 'A Plataforma',
    pageAboutThePlatformA: 'O projeto é resultado da insatisfação de parceiros Awaete e não indígenas em relação a acessibilidade e participacao das publicações, pesquisas e seus resultados bem como suas metodologias de relacionamento e busca o retorno organizado por pesquisadores Awaete em plataforma em seu idioma. Compõem o acervo arquivos em diversos formatos de mídia coletados apartir de anos de pesquisa de Timei Assurini e Carla Romano que durante o circuito troca de saberes e práticas com o projeto Agenda Awaete junto a rede de apoiadores coletaram publicações das mais diversas.',
    pageAboutThePlatformB: 'A plataforma conta também com produções autônomas contemporâneas do coletivo da família  Marytykwawara.',
    pageAboutThePlatformC: 'Tem algum arquivo ou publicação sobre nós? Envia pra gente, pelo e-mail janerkaawaete@gmail.com e colabore ainda mais para a cocriação de metodologias decoloniais de pesquisa e diálogo com povos ancestrais!',
    pageTitleAboutTeam: 'Equipe técnica',
    pageTitleRoleTranslation: 'Produção Cultural e Tradução',
    pageTitleRoleExecutiveManagement: 'Produção Cultural e Direção Executiva',
    pageTitleRoleDesigner: 'Designers',
    pageTitleRoleDev: 'Desenvolvedores',
    pageTitleAboutTeamRealization: 'Produção da Plataforma de Acervo Multimídia'
  },
  admin: {
    alertPublicationError: 'Erro durante publicação. Arquivo não publicado.', // Pending translation
    alertPublicationSuccess: 'Arquivo publicado com sucesso.', // Pending translation
    alertRejectionError: 'Error durante rejeição de arquivo. Arquivo não deletado.', // Pending translation
    alertRejectionSuccess: 'Arquivo rejeitado. Deleção concluída.', // Pending translation
    buttonLabelAccept: 'Iemipyra',
    buttonLabelReject: 'Itigupyry\'yma',
    pageTextAdminLabel: 'Administrador', // Pending translation
    pageTextSubmissionsLabel: 'Etxakawuma',
    pageTextSureToPublish: 'Tem certeza que deseja APROVAR a submissão?', // Pending translation
    pageTextSureToReject: 'Tem certeza que deseja REJEITAR a submissão? O arquivo será deletado.' // Pending translation
  },
  construction: {
    pageTextTitle: 'Essa página está em construção...' // Pending translation
  },
  contact: {
    buttonLabelSubmit: 'Enviar', // Pending translation
    formFieldLabelName: 'Nome', // Pending translation
    formFieldLabelPhone: 'Telefone', // Pending translation
    formFieldLabelEmail: 'E-mail', // Pending translation
    formFieldLabelMessage: 'Escreva uma mensagem', // Pending translation
    formValidationFieldRequired: 'Por favor, preencha este campo', // Pending translation
    pageTextTitle: 'Tem algum arquivo sobre a etnia Awaete?', // Pending translation
    pageTextSubtitle: 'Entre em contato conosco' // Pending translation
  },
  error404: {
    pageTextTitle: 'Essa página não existe...', // Pending translation
    buttonLabelReturnHome: 'Ir para página inicial' // Pending translation
  },
  footer: {
    buttonLabelSubmissionTerms: 'Karupi\'ejaygawi emut',
    buttonLabelAdminArea: 'Ijara apa',
    imageAltTextJanerakaLogo: 'Logo Instituto Janereka', // Pending translation
    contact: 'Contato' // Pending translation
  },
  gallery: {
    alertDeleteFailed: 'Ocorreu um erro na deleção do arquivo.', // Pending translation
    alertDeleteSuccess: 'Arquivo deletado com sucesso.', // Pending translation
    alertGenericError: 'Ocorreu um erro.', // Pending translation
    alertSubmissionSuccess: 'Seu arquivo foi submetido com sucesso.', // Pending translation
    alertUpdateFailed: 'Ocorreu um erro na atualização do arquivo.', // Pending translation
    alertUpdateSuccess: 'Seu aquivo foi atualizado com sucesso.', // Pending translation
    buttonLabelCancelEdit: 'Cancelar edição', // Pending translation
    buttonLabelClose: 'Fechar', // Pending translation
    buttonLabelDelete: 'Deletar', // Pending translation
    buttonLabelEdit: 'Editar detalhes', // Pending translation
    buttonLabelSend: 'Emana',
    buttonLabelSubmitEdit: 'Submeter mudanças', // Pending translation
    categoryLabelAll: 'Todos', // Pending translation
    categoryLabelAudio: 'Janejeyga ryruruma',
    categoryLabelFiles: 'Janemarytykwara ryruruma',
    categoryLabelImages: 'Janeraygawa ryruruma',
    categoryLabelVideos: 'Janeyga ryruruma',
    mainTableTitle: 'Acervo', // Pending translation
    mediaDetailsAuthor: 'Autor:', // Pending translation
    mediaDetailsAuthorIndigenousGroup: 'Aldeia do autor:', // Pending translation
    mediaDetailsCreated: 'Criado em:', // Pending translation
    mediaDetailsDescription: 'Descrição:', // Pending translation
    mediaDetailsUploader: 'Enviado por:', // Pending translation
    menuSortAlphabetical: 'iapa\'katuteawa',
    menuSortBy: 'etxak\'katuteawa',
    menuSortNewer: 'kumeti\'u\'u ',
    menuSortOlder: 'myweuarera',
    menuSortHashtags: 'ikuapawuma',
    pageTextSureToDelete: 'Proceder com deleção do arquivo?', // Pending translation
    paginationOfPage: 'de', // Pending translation
    paginationRowsPerPageLabel: 'ymyry ne:',
    searchBarPlaceholder: 'ekat\'karupi'
  },
  home: {
    buttonLabelGalleryAccess: 'Karupi\'etxak',
    imageAltTextCoverPicture: 'Pessoa Indígena pulando no rio', // Pending translation
    pageTitle: 'Janereka Ryru', // Pending translation
    pageSubtitle: 'Plataforma de Salvaguarda e Acervo Multimídia do Patrimônio Awaete', // Pending translation
    pageTextProjectFunding: 'Marytykwaawa uruapa uruta karupi lei federal aoa kaga tupi 14.017/20 Mywe uarera pemumeu ukwat gy iymynera upe ii lei Aldir Blanc para puro auga erê 2020',
    pageTextAboutTitle: 'Sobre a Plataforma', // Pending translation
    pageTextAboutLine1: 'Karupi gy iymynera gy marytykwaawawe Apa gymarakapera i\'i urumuny\'yk né',
    pageTextAboutLine2: 'Tumue gy ure jawu urue'
  },
  login: {
    alertLoginFailed: 'Não foi possível fazer login. Tente novamente.', // Pending translation
    alertHasNoPerms: 'Você não possui permissão para acessar essa área.', // Pending translation
    alertLoginNecessary: 'Login necessário para acessar a plataforma.', // Pending translation
    alertSessionAboutToExpire: 'Sua sessão irá expirar em breve. Por favor, refaça o login para continuar usando a plataforma.', // Pending translation
    buttonLabelEnter: 'Entrar', // Pending translation
    formFieldLabelUsername: 'Nome', // Pending translation
    formFieldLabelPassword: 'Senha', // Pending translation
    formValidationFieldRequired: 'Este campo é necessário', // Pending translation
    pageTextTitle: 'Para acessar o acervo, entre com o login e senha', // Pending translation
    pageTextSubtitle: 'Acesso exclusivo para membros da aldeia' // Pending translation
  },
  menus: {
    buttonLabelCancel: 'Cancelar', // Pending translation
    buttonLabelFinish: 'Finish', // Pending translation
    buttonLabelLogin: 'Login', // Pending translation
    buttonLabelLogout: 'E\'m',
    buttonLabelNo: 'Não', // Pending translation
    buttonLabelYes: 'Sim', // Pending translation
    langSwitch: 'Jeyga',
    navigationAbout: 'Karupi\'etxa',
    navigationAdmin: 'Admin', // Pending translation
    navigationContact: 'Contato', // Pending translation
    navigationGallery: 'Janereka Ryru ruma',
    navigationHome: 'Ypy',
    navigationUserArea: 'Perfil' // Pending translation
  },
  submission: {
    alertInvalidFile: 'O arquivo selecionado é inválido. Tente outro formato.', // Pending translation
    alertMustAcceptTerms: 'É necessário aceitar os termos de submissão para enviar o arquivo.', // Pending translation
    alertNoFileSelected: 'Nenhum arquivo selecionado para envio.', // Pending translation
    alertSubmissionError: 'Ocorreu um erro na submissão do arquivo. Tente novamente.', // Pending translation
    alertSubmissionSuccess: 'Arquivo submetido com sucesso.', // Pending translation
    buttonLabelNewAuthor: 'Criar novo autor', // Pending translation
    buttonLabelNewFile: 'iyau',
    buttonLabelSend: 'imana',
    formFieldHintHastags: 'Separe cada tag com uma vírgula (,).\nEx: tag1, tag2, tag3', // Pending translation
    formFieldLabelAcceptTerms: 'Li e aceito os termos de submissão da plataforma', // Pending translation
    formFieldLabelAuthorName: 'etxakame\'ara:',
    formFieldLabelAuthorOrigin: 'ereramepe\'ejera nugane:',
    formFieldLabelDescription: 'ekuatxa\'au:',
    formFieldLabelFilename: 'nemarytykuara\'rera:',
    formFieldLabelFilepicker: 'Selecione um arquivo', // Pending translation
    formFieldLabelHashtags: 'ekuatxa\'au:',
    formFieldPlaceholderDescription: 'Insira uma descrição do seu material aqui', // Pending translation
    formFieldPlaceholderFilename: 'Insira o nome do seu arquivo aqui', // Pending translation
    formFieldPlaceholderNameAuthor: 'Insira o seu nome', // Pending translation
    formSectionTitleAuthor: 'ejera enum', // Pending translation
    formTextNotAnAuthor: 'Ainda não é um autor?', // Pending translation
    formValidationFieldRequired: 'Este campo é necessário.', // Pending translation
    formValidationTagsNumber: 'Por favor, adicione ao menos uma tag.' // Pending translation
  },
  user: {
    galleryCategoryLabelPersonal: 'Acervo pessoal', // Pending translation
    galleryCategoryLabelSubmissions: 'Submissões', // Pending translation
    pageTextFamily: 'Jeretarera:',
    pageTextIndigenousGroup: 'Jetawa\'rera:',
    isFilePublished: 'ARQUIVO AINDA NÃO FOI ACEITO PARA PUBLICAÇÃO'
  }
}
